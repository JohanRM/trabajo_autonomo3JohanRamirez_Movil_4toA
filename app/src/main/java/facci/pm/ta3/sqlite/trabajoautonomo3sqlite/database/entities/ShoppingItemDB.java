package facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.entities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.helper.ShoppingElementHelper;
import facci.pm.ta3.sqlite.trabajoautonomo3sqlite.database.model.ShoppingItem;
import java.util.ArrayList;

public class ShoppingItemDB {

//Ramirez Johan

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    private ShoppingElementHelper dbHelper;

    public ShoppingItemDB(Context context) {
        // Create new helper
        dbHelper = new ShoppingElementHelper(context);
    }

    /* Inner class that defines the table contents */
    public static abstract class ShoppingElementEntry implements BaseColumns {
        public static final String TABLE_NAME = "entry";
        public static final String COLUMN_NAME_TITLE = "title";

        public static final String CREATE_TABLE = "CREATE TABLE " +
                TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
                COLUMN_NAME_TITLE + TEXT_TYPE + " )";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }
//Ramirez Johan

    public void insertElement(String productName) {
        //TODO: Todo el código necesario para INSERTAR un Item a la Base de datos
        //Aqui se obtiene el repositorio de datos en modo write osea de escritura
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // Se Crea un mapa de valores
        ContentValues values = new ContentValues();
        //Aqui podemos ingresar los nombres de los producots
        values.put(ShoppingElementEntry.COLUMN_NAME_TITLE,productName);
        // Insertar la nueva fila, devolviendo el valor de la clave primaria de la nueva fila
        db.insert(ShoppingElementEntry.TABLE_NAME, null, values);

    }

//Ramirez Johan
    public ArrayList<ShoppingItem> getAllItems() {

        ArrayList<ShoppingItem> shoppingItems = new ArrayList<>();

        String[] allColumns = { ShoppingElementEntry._ID,
            ShoppingElementEntry.COLUMN_NAME_TITLE};

        Cursor cursor = dbHelper.getReadableDatabase().query(
            ShoppingElementEntry.TABLE_NAME,    // The table to query
            allColumns,                         // The columns to return
            null,                               // The columns for the WHERE clause
            null,                               // The values for the WHERE clause
            null,                               // don't group the rows
            null,                               // don't filter by row groups
            null                                // The sort order
        );

        cursor.moveToFirst();
//Ramirez Johan
        while (!cursor.isAfterLast()) {
            ShoppingItem shoppingItem = new ShoppingItem(getItemId(cursor), getItemName(cursor));
            shoppingItems.add(shoppingItem);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        dbHelper.getReadableDatabase().close();
        return shoppingItems;
    }
//Ramirez Johan
    private long getItemId(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndexOrThrow(ShoppingElementEntry._ID));
    }

    private String getItemName(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndexOrThrow(ShoppingElementEntry.COLUMN_NAME_TITLE));
    }


    public void clearAllItems() {
        //TODO: Todo el código necesario para ELIMINAR todos los Items de la Base de datos
        // Obtiene el repositorio de datos en modo de escritura
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // Emitir instrucción SQL
        db.delete(ShoppingElementEntry.TABLE_NAME,null,null);
    }

    public void updateItem(ShoppingItem shoppingItem) {
        //TODO: Todo el código necesario para ACTUALIZAR un Item en la Base de datos
        // Obtiene el repositorio de datos en modo de escritura
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // Nuevo valor para una columna
        ContentValues values = new ContentValues();
        //inserta los titulos del producto
        values.put(ShoppingElementEntry.COLUMN_NAME_TITLE,shoppingItem.getName());
        // Actualizar la fila basado en el titulo del producto
        String selection = ShoppingElementEntry._ID + " = ?";
        // Especifica los argumentos en el orden de marcador de posición
        String[] selectionArgs = {String.valueOf(shoppingItem.getId())};
        // Emite instrucción SQL la de actualizar la lista
        db.update(ShoppingElementEntry.TABLE_NAME,values,selection,selectionArgs);



    }
// Ramirez Johan
    public void deleteItem(ShoppingItem shoppingItem) {
        //TODO: Todo el código necesario para ELIMINAR un Item de la Base de datos
        // Obtiene el repositorio de datos en modo de lectura
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // Define donde parte de la consulta de la lista de producto de la fila
        String selection = ShoppingElementEntry.COLUMN_NAME_TITLE + " = ?";
        // Especifica los argumentos en el orden de marcador de posición
        String[] selectionArgs = {shoppingItem.getName()};
        // Emite instrucción SQL la de eliminar la lista
        db.delete(ShoppingElementEntry.TABLE_NAME, selection, selectionArgs);
    }
}
//Ramirez Johan